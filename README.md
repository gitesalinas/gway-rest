# gway-rest

Es un proyecto basado en el algoritmo de Dijkstra para la b�squeda de la ruta mas corta entre dos puntos.

Al ejecutar el proyecto habilita los servicios Web/Rest.

La url del panel web es htpp://localhost:8080/
La url del servicio rest es htpp://localhost:8080/ruta-mas-corta

El servicio rest s�lo acepta peticiones POST y los datos a enviar son:

sourceLat -> Latitud de punto incio
sourceLon -> Longitud de punto incio
targetLat -> Latitud de punto objetivo
targetLon -> Longitud de punto objetivo

, son enviados en formato JSON.

Si existen problemas con el n�mero de puerto puede facilmente ser cambiado 
en el archivo de propiedades ubicado en src/main/resources/application/properties.

El presente proyecto solo contempla el core de la funcionalidad y la 
interacci�n con el usuario a trav�s de un panel web.

Dijkstra, Grafos, Binary Heap, Cola de Prioridad, Geometr�a, Geograf�a

![Screenshot](image.png)

## Installation

El proyecto esta desarrollado en el IDE Eclipse Oxygen

Eclipse IDE for Enterprise Java Developers.
Version: 2018-12 (4.10.0)
Build id: 20181214-0600

Java 8

```bash
Importar proyecto a Eclipse
Actualizar Maven
Descomprimir ShapeFile (SF.zip) en un folder
Modificar ruta de ShapeFiles (com.devnull.gway.dao.connection.ShapeFileDataStore)
```

## Usage

```java
Arrastra los markers
```

## Autores

* **David Mera D�vila** - *Investigador*
* **Eder Salinas Acosta** - *Investigador*
* **Lourdes Roxana Diaz** - *Asesora*

Proyecto desarrollado para la sustentci�n de t�sis.