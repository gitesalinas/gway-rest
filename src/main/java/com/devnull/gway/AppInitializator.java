package com.devnull.gway;



import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.devnull.gway.memory.MemoryGraph;

@Component
class AppInitializator {
 
	/***
	 * Inyección de dependecias
	 */
	@Autowired
	private MemoryGraph memoryGraph;
	
    @PostConstruct
    private void init() throws Exception {
    	/***
    	 * Carga de grafo
    	 */
    	memoryGraph.load();
    }
}
