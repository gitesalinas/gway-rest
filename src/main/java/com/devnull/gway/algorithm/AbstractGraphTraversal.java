package com.devnull.gway.algorithm;

import java.util.Comparator;
import java.util.LinkedHashMap;

import com.devnull.gway.entity.Graph;
import com.devnull.gway.entity.GraphNode;
import com.devnull.gway.entity.Node;
import com.devnull.gway.entity.Path;
import com.devnull.gway.structure.Queue;

public abstract class AbstractGraphTraversal {

	private LinkedHashMap<Node, GraphNode> nodesMap = new LinkedHashMap<>();
	private Queue<GraphNode> queue;
	private GraphNode graphSourceNode;
	private Comparator<GraphNode> comparator;
	private Graph graph;
	
	public AbstractGraphTraversal(Graph graph, Comparator<GraphNode> comparator) {
		this.graph = graph;
		this.comparator = comparator;
		this.queue = new Queue<>(comparator);
	}
	
	public LinkedHashMap<Node, GraphNode> getNodesMap() {
		return nodesMap;
	}
	
	public GraphNode getNext() {
		return queue.poll();
	}

	public void init() {
		this.nodesMap.clear();
		
		this.queue = new Queue<GraphNode>(this.comparator);
		
		this.nodesMap.put(this.graphSourceNode.getNode(), this.graphSourceNode);
		
		this.queue.push(this.graphSourceNode);
	}
	
	public abstract Path getPath(Node targetNode);
	public abstract Path calculate(Node sourceNode, Node targetNode);
	
 	public Node getSourceNode() {
 		if(null == graphSourceNode) return null;
 		
		return graphSourceNode.getNode();
	}
	
	public void setSourceNode(Node sourceNode) {
		this.graphSourceNode = new GraphNode(sourceNode, null, 0.0, false);
	}

	public Queue<GraphNode> getQueue() {
		return this.queue;
	}
	
	public void setQueue(Queue<GraphNode> queue) {
		this.queue = queue;
	}

	public Graph getGraph() {
		return graph;
	}

	protected void getPathRecursive(Path path, GraphNode graphNode) {
		
		if(graphNode == null) return;
		
		getPathRecursive(path, graphNode.getParent());
		
		path.add(getNodesMap().get(graphNode.getNode()));
	}
}
