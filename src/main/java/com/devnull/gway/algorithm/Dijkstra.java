package com.devnull.gway.algorithm;

import java.util.Comparator;

import com.devnull.gway.entity.Edge;
import com.devnull.gway.entity.Graph;
import com.devnull.gway.entity.GraphNode;
import com.devnull.gway.entity.Node;
import com.devnull.gway.entity.Path;


public class Dijkstra extends AbstractGraphTraversal {
	
	/***
	 * Comparador por defecto usado por cola de prioridad
	 */
	private static final Comparator<GraphNode> COMPARATOR = new Comparator<GraphNode>() {
		@Override
		public int compare(GraphNode graphNode1, GraphNode graphNode2) {
			return graphNode1.getWeightAccumulated().compareTo(graphNode2.getWeightAccumulated());
		}
	};
	
	
	public Dijkstra(Graph graph, Comparator<GraphNode> comparator) {
		super(graph, (comparator == null ? COMPARATOR : comparator));
	}
	
	/***
	 * Retorna un objeto de tipo Path que es un ArrayList 
	 * de todos los puntos que conforman la ruta mas corta
	 */
	@Override
	public Path getPath(Node targetNode) {
		
		Path path = new Path();
		
		GraphNode graphNode = super.getNodesMap().get(targetNode);
		
		this.getPathRecursive(path, graphNode);
		
		return path;
	}
	
	/***
	 * Calcula la ruta mas corta desde el punto inicio hasta el punto objetivo
	 * y prepara los objetos necesarios previo a la ejecución del algoritmo.
	 * Retorna todos los puntos que conforman la ruta mas corta
	 */
	@Override
	public Path calculate(Node sourceNode, Node targetNode) {
		if(null == super.getSourceNode() || !super.getSourceNode().equals(sourceNode)) {
			super.setSourceNode(sourceNode);
		}
		
		this.init();
		
		execute(targetNode);
		
		return this.getPath(targetNode);
	}
	
	private void execute(Node targetNode) {
		
		int vistedNodes = 0;
		
		while(!super.getQueue().isEmpty()) {
			
			/***
			 * GraphNode clase envoltoria que contiene a un Nodo normal
			 * En esa clase se guarda información del peso y una referencia
			 * al nodo dentro de la ruta óptima previo a este
			 */
			GraphNode currentGraphNode = super.getQueue().poll();
			
			/***
			 * Nodo actual
			 */
			Node currentNode = currentGraphNode.getNode();
		
			/***
			 * Se visitó nodo. El peso que tiene para llegar es el óptimo
			 */
			currentGraphNode.setIsVisited(true);
			
			/***
			 * Examinar cada arista del nodo visitado currentNode.getEdges()
			 */
			for(Long key : currentNode.getEdges().keySet()) {
				
				Edge edge = currentNode.getEdges().get(key);
				
				/***
				 * Nodo adjacente del ari evaluada
				 */
				Node adjacentNode = edge.getTargetNode();
				
				GraphNode adjacentGraphNode = super.getNodesMap().get(adjacentNode);
				
				/***
				 * Verifica si el nodo adjacente es un candidato nuevo 
				 * para agregar a la cola de prioridad
				 */
				if(check(currentGraphNode, adjacentGraphNode, edge)) {
					
					if(null == adjacentGraphNode) {
						/***
						 * Se agrega un nuevo nodo a la cola de prioridad
						 */
						Add(currentGraphNode, adjacentNode, edge);
					
					} else {
						/***
						 * Se actualiza el peso para llegar al nodo adjacente
						 */
						Update(currentGraphNode, adjacentGraphNode, edge);
						
					}
				}
			}
			
			/***
			 * Número de nodos visitados
			 */
			vistedNodes++;
			
			/***
			 * Se encontró el nodo objetivo con el camino mas corto
			 */
			if(currentNode.equals(targetNode)) {
				break;
			}
		}
		
		System.out.println("Nodos visitados:" + vistedNodes);
	}
	
	/***
	 * Verifica si el nodo adjacente es un candidato nuevo para agregar 
	 * a la cola de prioridad o si existe un camino mas corto para llegar
	 * @return Retorna verdadero si se procesará
	 */
	private boolean check(GraphNode currentGraphNode, GraphNode adjacentGraphNode, Edge edge) {
		
		boolean adjacentGraphNodeIsVisited = false;
		if(null != adjacentGraphNode) adjacentGraphNodeIsVisited = adjacentGraphNode.getIsVisited();
		
		Double adjacentGraphNodeWeight = 0.0;
		if(null != adjacentGraphNode) adjacentGraphNodeWeight = adjacentGraphNode.getWeightAccumulated();
		
		Double newWeight = currentGraphNode.getWeightAccumulated() + edge.getWeight();
		
		return (null == adjacentGraphNode || (!adjacentGraphNodeIsVisited && adjacentGraphNodeWeight > newWeight));
	}
	
	/***
	 * Candidato nuevo para agregar a la cola de prioridad
	 */
	private void Add(GraphNode currentGraphNode, Node adjacentNode, Edge edge) {
		
		Double weight = currentGraphNode.getWeightAccumulated() + edge.getWeight();
		
		super.getNodesMap().put(adjacentNode, new GraphNode(adjacentNode, currentGraphNode, weight, false));
		super.getQueue().push(super.getNodesMap().get(adjacentNode));
	}
	
	/***
	 * Existe un camino mas corto para llegar
	 */
	private void Update(GraphNode currentGraphNode, GraphNode adjacentGraphNode, Edge edge) {
		
		adjacentGraphNode.setParent(currentGraphNode);
		adjacentGraphNode.setWeightAccumulated(currentGraphNode.getWeightAccumulated() + edge.getWeight());
		super.getQueue().relax(adjacentGraphNode);
	}
}
