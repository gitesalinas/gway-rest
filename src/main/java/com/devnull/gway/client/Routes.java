package com.devnull.gway.client;

import java.util.Iterator;
import java.util.List;

import org.geotools.geometry.jts.JTSFactoryFinder;

import com.devnull.gway.algorithm.Dijkstra;
import com.devnull.gway.entity.Edge;
import com.devnull.gway.entity.Graph;
import com.devnull.gway.entity.GraphNode;
import com.devnull.gway.entity.Node;
import com.devnull.gway.entity.Path;
import com.devnull.gway.entity.ResponsePoint;
import com.devnull.gway.entity.ResponseRoute;
import com.devnull.gway.util.GraphQuery;
import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.geom.GeometryFactory;
import com.vividsolutions.jts.geom.LineString;
import com.vividsolutions.jts.geom.Point;
import com.vividsolutions.jts.operation.distance.DistanceOp;

public class Routes {
	
	/***
	 * Mínima distancia que indica que un punto esta dentro de una línea
	 */
	private static final double MIN_DISTANCE_TRESHOLD = 0.00001D;
	/***
	 * Fabrica de objetos geométricos util para cálculos de distancia entre puntos, lineas
	 */
	private static final GeometryFactory GEOMETRYFACTORY = JTSFactoryFinder.getGeometryFactory();
	
	private Edge[] nearestEdges = new Edge[2];
	
	
	private Node sourceNode;
	private Node targetNode;
	
	private Point sourcePoint;
	private Point targetPoint;
	private Point newSourcePoint;
	private Point newTargetPoint;
	
	private Graph gwGraph;
	
	private ResponseRoute responseRoute = new ResponseRoute();
	
	public Routes(Graph gwGraph, Point sourcePoint, Point targetPoint) {
		this.gwGraph = gwGraph;
		this.sourcePoint = sourcePoint;
		this.targetPoint = targetPoint;
	}
	
	public ResponseRoute execute() throws Exception {
		this.calculateEdgesNearestToPoints();
		this.runAlgorithm();
		this.calculateNewPoints();
		this.resolveNewPoints(this.responseRoute.getPoints());
		
		return this.responseRoute;
	}
	
	/***
	 * Cálcula aristas mas cercanas y los nodos segundo y penultimo
	 * 
	 */
	private void calculateEdgesNearestToPoints() throws Exception {
		
		/***
		 * arista mas cercana al punto inicio nearestEdges[0]
		 * arista mas cercana al punto objetivo nearestEdges[1]
		 */
		this.nearestEdges = getEdgesNearToPoints(this.sourcePoint, this.targetPoint);
		
		if(null == this.nearestEdges[0] || null == this.nearestEdges[1]) { 
			throw new Exception("No Edges Found");
		}

		this.sourceNode = this.defineSourceNode(nearestEdges[0]);
		this.targetNode = this.defineTargetNode(nearestEdges[1]);
	}
	
	/**
	 * @param pointSource
	 * @param pointTarget
	 * @return - [0] Nearest edge to pointSource, [1] Nearest edge to pointTarget
	 */
	private Edge[] getEdgesNearToPoints(Point sourcePoint, Point targetPoint) {
		
		/***
		 * Busca en el grafo las aristas mas cercanas a los puntos inicio y objetivo
		 */
		Graph.IGraphQuery<Edge, Edge[]> query = 
				GraphQuery.getQueryFindGraphEdgesBySourceAndTargetPoints(sourcePoint, targetPoint);
		
		gwGraph.queryGraphEdges(query);
		
		return query.getValue();
	}
	
	/***
	 * Obtiene el nodo penultimo
	 */
	private Node defineTargetNode(Edge edge) {
		if(edge.getIsBidirectional()) {
			Point sSourcePoint = (Point)edge.getSourceNode().getObject();
			Point tSourcePoint = (Point)edge.getTargetNode().getObject();
			
			if(this.targetPoint.distance(sSourcePoint) 
					> this.targetPoint.distance(tSourcePoint))
				return edge.getTargetNode();
			
			return edge.getSourceNode();
		}
		
		//return edge.getTargetNode();
		return edge.getSourceNode();
	}
	
	/***
	 * Obtiene el nodo segundo
	 */
	private Node defineSourceNode(Edge edge) {
		if(edge.getIsBidirectional()) {
			Point sSourcePoint = (Point)edge.getSourceNode().getObject();
			Point tSourcePoint = (Point)edge.getTargetNode().getObject();
			
			if(this.sourcePoint.distance(sSourcePoint) 
					<= this.sourcePoint.distance(tSourcePoint))
				return edge.getSourceNode();
			
			return edge.getTargetNode();
		}
		
		//return edge.getSourceNode();
		return edge.getTargetNode();
	}
	
	
	
	/***
	 * Ejectua algoritmo y obtiene la ruta mas corta 
	 * colocandola dentro del objeto respuesta
	 */
	private void runAlgorithm() throws Exception {
		
		Dijkstra algorithm = new Dijkstra(gwGraph, null);
		
		/***
		 * Obtiene el camino mas corto entre dos puntos.
		 * Se recorre el resultado recursivamente para mostrar
		 * la ruta mas corta desde nodo inicio a objetivo. Esto
		 * se hace dentro del metodo calculate tamabién.
		 */
		Path path = (Path)algorithm.calculate(this.sourceNode, targetNode);
		
		if(null == path || path.size() <= 0) {
			throw new Exception("No Route Found");
		}
		
		this.responseRoute.setMessage("Success");
		this.responseRoute.setResult("Ok");
		
		double distance = 0.0;
		
		//GraphNode graphNodeTemp = null;
		
		for(Iterator<?> i = path.getIterator(); i.hasNext();) {
			
			GraphNode graphNode = (GraphNode)i.next();
			
			double weight = Math.round(graphNode.getWeightAccumulated() * 100.00) / 100.00;
			
			//if(null != graphNodeTemp) {
				//weight = graphNode.getWeightAccumulated() - graphNodeTemp.getWeightAccumulated();
			//}
			
			Coordinate coordinate = ((Point)graphNode.getNode().getObject()).getCoordinate();
			
			this.responseRoute.getPoints().add(new ResponsePoint(coordinate.y, coordinate.x, weight));
			
			if(!i.hasNext()) {
				/***
				 * Para darle formato de dos decimales
				 */
				distance = weight;
			}
			
			//graphNodeTemp = graphNode;
		}
		
		this.responseRoute.setDistance(distance);
	}
	
	
	/***
	 * El punto mas cerca desde el punto clickeado en el mapa a la arista mas cerca.
	 * Crear un punto no existente dentro de la topología de carreteras, vías y pistas
	 * de Perú
	 * @throws Exception
	 */
	private void calculateNewPoints() throws Exception {
		
		LineString sourceLineString = ((LineString)this.nearestEdges[0].getObject());
		LineString targetLineString = ((LineString)this.nearestEdges[1].getObject());
		
		DistanceOp sourceCalculator = new DistanceOp(sourceLineString, sourcePoint);
		DistanceOp targetCalculator = new DistanceOp(targetLineString, targetPoint);
		
		this.newSourcePoint = (Point)createPoint(sourceCalculator.nearestPoints()[0]);
		this.newTargetPoint = (Point)createPoint(targetCalculator.nearestPoints()[0]);
		
		if(null == this.newSourcePoint || null == this.newTargetPoint) {
			throw new Exception("Error Calculating New Points");
		}
	}
	

	/***
	 * Ubica en la lista que contiene la ruta mas corta los nuevos puntos calculados
	 */
	private void resolveNewPoints(List<ResponsePoint> points) throws Exception {
		System.out.println("Point size: " + points.size());
		
		/***
		 * Si seleccione un mismo punto, solo se agrega en la lista que contiene
		 * la ruta mas corta el punto nuevo de inicio y el punto nuevo final
		 */
		if(1 == points.size()) {
			points.add(0, new ResponsePoint(this.newSourcePoint.getCoordinate().y, this.newSourcePoint.getCoordinate().x, 0.0));
			points.add(2, new ResponsePoint(this.newTargetPoint.getCoordinate().y, this.newTargetPoint.getCoordinate().x, 0.0));
			
			return;
		}
		
		Double x1 = points.get(0).getLongitude();
		Double y1 = points.get(0).getLatitude();
		Double x2 = points.get(1).getLongitude();
		Double y2 = points.get(1).getLatitude();
		
		/***
		 * Si el nuevo punto esta dentro de la linea creada entre el primer y segundo punto.
		 * Si es así se actualiza la información del primer punto
		 * Sino se agrega un nuevo punto como si fuera el primero de la lista
		 */
		LineString startLineString = (LineString)createLineString(new Coordinate(x1, y1), new Coordinate(x2, y2));
		if(newSourcePoint.distance(startLineString) <= MIN_DISTANCE_TRESHOLD) {
			points.get(0).setLatitude(this.newSourcePoint.getCoordinate().y);
			points.get(0).setLongitude(this.newSourcePoint.getCoordinate().x);
		} 
		else { points.add(0, new ResponsePoint(this.newSourcePoint.getCoordinate().y, this.newSourcePoint.getCoordinate().x, 0.0)); }
		
		
		Double x3 = points.get(points.size() - 1).getLongitude();
		Double y3 = points.get(points.size() - 1).getLatitude();
		Double x4 = points.get(points.size() - 2).getLongitude();
		Double y4 = points.get(points.size() - 2).getLatitude();
		
		/***
		 * La misma lógica anterior pero tomando en cuenta los puntos último y penultimo
		 */
		LineString endLineString = (LineString)createLineString(new Coordinate(x3, y3), new Coordinate(x4, y4));
		if(newTargetPoint.distance(endLineString) <= MIN_DISTANCE_TRESHOLD) {
			points.get(points.size() - 1).setLatitude(this.newTargetPoint.getCoordinate().y);
			points.get(points.size() - 1).setLongitude(this.newTargetPoint.getCoordinate().x);
		} 
		else { points.add(new ResponsePoint(this.newTargetPoint.getCoordinate().y, this.newTargetPoint.getCoordinate().x, 0.0)); }	
	}
	
	
	
	//Metodo util para crear objectos de tipo Punto
	private Geometry createPoint(Coordinate coordinate) {
		return GEOMETRYFACTORY.createPoint(coordinate);
	}
	
	//Metodo util para crear objectos de tipo LineString
	private Geometry createLineString(Coordinate coordinate1, Coordinate coordinate2) {
		return GEOMETRYFACTORY.createLineString(new Coordinate[] { coordinate1, coordinate2 });
	}
}
