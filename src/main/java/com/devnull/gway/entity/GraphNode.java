package com.devnull.gway.entity;

/***
 * Clase que envuelve a Node para guardar información extra 
 * de tal forma el objeto original no sufra cambios y sea 
 * reutilizado en próximas búsquedas
 */
public class GraphNode {

	private Node node;
	private GraphNode parent;
	private Double weightAccumulated;

	private Boolean isVisited;

	public GraphNode() {
		this(null, null, Double.MAX_VALUE, false);
	}

	public GraphNode(Node node, GraphNode parent, Double weightAccumulated, Boolean isVisited) {
		this.node = node;
		this.parent = parent;
		this.weightAccumulated = weightAccumulated;
		this.isVisited = isVisited;
	}

	public Node getNode() {
		return node;
	}

	public void setNode(Node node) {
		this.node = node;
	}

	public GraphNode getParent() {
		return parent;
	}

	public void setParent(GraphNode parent) {
		this.parent = parent;
	}

	public Double getWeightAccumulated() {
		return weightAccumulated;
	}

	public void setWeightAccumulated(Double weightAccumulated) {
		this.weightAccumulated = weightAccumulated;
	}

	public Boolean getIsVisited() {
		return isVisited;
	}

	public void setIsVisited(Boolean isVisited) {
		this.isVisited = isVisited;
	}
}