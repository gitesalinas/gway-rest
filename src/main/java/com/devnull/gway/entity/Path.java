package com.devnull.gway.entity;

import java.util.ArrayList;
import java.util.Iterator;

/***
 * ArrayList que guardara los nodos que conformarán la ruta mas corta.
 * Es necesario recorrer recursivamente para obtener la ruta mas corta ordenada.
 */
public class Path extends ArrayList {
	public Path() {
		super();
	}

	public void add(Node node) {
		super.add(node);
	}

	public Iterator getIterator() {
		return super.iterator();
	}
}
