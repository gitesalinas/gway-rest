package com.devnull.gway.entity;

/**
 * Arista
 */
public class Edge {

	public static final String FORWARD = "FORWARD";
	public static final String REVERSE = "REVERSE";

	private long id;
	private String name;
	private Node sourceNode;
	private Node targetNode;
	private double weight;
	private Object object;
	private Object data;

	/**
	 * It is bidirectional when exist a reverse cost different to 1000000.0
	 */
	private boolean isBidirectional = false;

	private String direction = FORWARD;

	public Edge(long id, Node sourceNode, Node targetNode, double weight) {
		this.id = id;
		this.sourceNode = sourceNode;
		this.targetNode = targetNode;
		this.weight = weight;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Node getSourceNode() {
		return sourceNode;
	}

	public void setSourceNode(Node sourceNode) {
		this.sourceNode = sourceNode;
	}

	public Node getTargetNode() {
		return targetNode;
	}

	public void setTargetNode(Node targetNode) {
		this.targetNode = targetNode;
	}

	public double getWeight() {
		return weight;
	}

	public void setWeight(double weight) {
		this.weight = weight;
	}

	public void setIsBidirectional() {
		this.isBidirectional = true;
	}

	public boolean getIsBidirectional() {
		return this.isBidirectional;
	}

	public void setDirection(String direction) {
		this.direction = direction;
	}

	public String getDirection() {
		return this.direction;
	}

	public Object getObject() {
		return object;
	}

	public void setObject(Object object) {
		this.object = object;
	}

	public Object getData() {
		return data;
	}

	public void setData(Object data) {
		this.data = data;
	}

	@Override
	public String toString() {
		return "GWayEdge [id=" + id + ", name=" + name + ", sourceNode=" + sourceNode + ", targetNode=" + targetNode
				+ ", weight=" + weight + ", object=" + object + "]";
	}
}
