package com.devnull.gway.entity;

/***
 * Representación de data en ShapeFile
 */
public class Segment {

	private int id;
	private long osm_id;
	private int clazz;
	private int flags;
	private int source;
	private int target;
	private Double km;
	private int kmh;
	private Double cost;
	private Double reverse_cost;
	private Double x1;
	private Double y1;
	private Double x2;
	private Double y2;

	public Segment() {
	}

	public Segment(Double km) {
		this.km = km;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public long getOsm_id() {
		return osm_id;
	}

	public void setOsm_id(long osm_id) {
		this.osm_id = osm_id;
	}


	public int getClazz() {
		return clazz;
	}

	public void setClazz(int clazz) {
		this.clazz = clazz;
	}

	public int getFlags() {
		return flags;
	}

	public void setFlags(int flags) {
		this.flags = flags;
	}

	public int getSource() {
		return source;
	}

	public void setSource(int source) {
		this.source = source;
	}

	public int getTarget() {
		return target;
	}

	public void setTarget(int target) {
		this.target = target;
	}

	public Double getKm() {
		return km;
	}

	public void setKm(Double km) {
		this.km = km;
	}

	public int getKmh() {
		return kmh;
	}

	public void setKmh(int kmh) {
		this.kmh = kmh;
	}

	public Double getCost() {
		return cost;
	}

	public void setCost(Double cost) {
		this.cost = cost;
	}

	public Double getReverse_cost() {
		return reverse_cost;
	}

	public void setReverse_cost(Double reverse_cost) {
		this.reverse_cost = reverse_cost;
	}

	public Double getX1() {
		return x1;
	}

	public void setX1(Double x1) {
		this.x1 = x1;
	}

	public Double getY1() {
		return y1;
	}

	public void setY1(Double y1) {
		this.y1 = y1;
	}

	public Double getX2() {
		return x2;
	}

	public void setX2(Double x2) {
		this.x2 = x2;
	}

	public Double getY2() {
		return y2;
	}

	public void setY2(Double y2) {
		this.y2 = y2;
	}
}
