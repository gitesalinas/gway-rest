package com.devnull.gway.entity;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Nodo
 */
public class Node {

	private Long id;
	private String name;
	private ConcurrentHashMap<Long, Edge> edges;
	private Object object;

	public Node(Long id) {
		this.id = id;
		this.edges = new ConcurrentHashMap<>();
	}

	public Node(Long id, String name) {
		this(id);
		this.name = name;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public ConcurrentHashMap<Long, Edge> getEdges() {
		return edges;
	}

	public void setEdges(ConcurrentHashMap<Long, Edge> edges) {
		this.edges = edges;
	}

	/**
	 * Get related Object
	 * 
	 * @return - It will be a Point object
	 */
	public Object getObject() {
		return object;
	}

	/**
	 * Set related Object
	 * 
	 * @param object - It should be a Point object
	 */
	public void setObject(Object object) {
		this.object = object;
	}

	@Override
	public String toString() {
		return "GWayNode [id=" + id + ", name=" + name + ", edges=" + edges.size() + ", object=" + object.toString()
				+ "]";
	}

	public void addEdge(Edge edge) {
		edges.put(edge.getId(), edge);
	}

	/**
	 * 
	 * @return - List of neighbours nodes
	 */
	public List<Node> getAdjacentNodes() {
		List<Node> nodes = new ArrayList<>();

		for (Long key : edges.keySet()) {
			nodes.add(edges.get(key).getTargetNode());
		}

		return nodes;
	}
}
