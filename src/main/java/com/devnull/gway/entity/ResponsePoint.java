package com.devnull.gway.entity;

/***
 * Cada punto que conforma la ruta mas corta
 */
public class ResponsePoint {
	private Double latitude;
	private Double longitude;
	private Double weight;

	public ResponsePoint() {
	}

	public ResponsePoint(Double latitude, Double longitude, Double weight) {
		this.latitude = latitude;
		this.longitude = longitude;
		this.weight = weight;
	}

	public Double getLatitude() {
		return latitude;
	}

	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}

	public Double getLongitude() {
		return longitude;
	}

	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}

	public Double getWeight() {
		return weight;
	}

	public void setWeight(Double weight) {
		this.weight = weight;
	}
}
