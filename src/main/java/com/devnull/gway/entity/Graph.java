package com.devnull.gway.entity;

import java.util.concurrent.ConcurrentHashMap;

/**
 * Grafo. Es utilizado para construir la topología de 
 * carreteras, pistas y víasde Perú. 
 * Contiene nodos y aristas.
 */
public class Graph {

	private String name;
	private ConcurrentHashMap<Long, Node> nodes;
	private ConcurrentHashMap<Long, Edge> edges;

	public Graph(String name) {
		this(name, new ConcurrentHashMap<>(), new ConcurrentHashMap<>());
	}

	/**
	 * 
	 * @param name  - The graph name
	 * @param nodes - Set of nodes
	 * @param edges - Set of edges
	 */
	public Graph(String name, ConcurrentHashMap<Long, Node> nodes, ConcurrentHashMap<Long, Edge> edges) {
		this.name = name;
		this.nodes = nodes;
		this.edges = edges;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public ConcurrentHashMap<Long, Node> getNodes() {
		return this.nodes;
	}

	public void setNodes(ConcurrentHashMap<Long, Node> nodes) {
		this.nodes = nodes;
	}

	public ConcurrentHashMap<Long, Edge> getEdges() {
		return this.edges;
	}

	public void setEdges(ConcurrentHashMap<Long, Edge> edges) {
		this.edges = edges;
	}

	@Override
	public String toString() {
		return "GWayGraph [vertexes=" + nodes.size() + ", edges=" + edges.size() + "]";
	}

	public void addNode(Node node) {
		this.nodes.put(node.getId(), node);
	}

	public void addEdge(Edge edge) {
		this.edges.put(edge.getId(), edge);
	}

	public Node getNodeById(Long id) {
		return nodes.get(id);
	}

	public Edge getEdgeById(Long id) {
		return edges.get(id);
	}

	/**
	 * 
	 * @param query - Conditionals to retrieve nodes as results
	 */
	public void queryGraphNodes(IGraphQuery query) {
		for (Long key : getNodes().keySet()) {
			query.find(getNodes().get(key));
			if (query.isDone())
				return;
		}
	}

	/**
	 * 
	 * @param query - Conditionals to retrieve nodes as results
	 */
	public void queryGraphEdges(IGraphQuery query) {
		for (Long key : getEdges().keySet()) {
			query.find(getEdges().get(key));
			if (query.isDone())
				return;
		}
	}

	/**
	 * Release memory used by the graph
	 */
	public void dispose() {
		this.name = null;
		if (null != nodes) {
			nodes.clear();
			nodes = null;
		}
		if (null != edges) {
			edges.clear();
			edges = null;
		}
	}

	public interface IDone {
		public boolean isDone();
	}

	/**
	 * Interface to generate graph queries
	 * 
	 * @author GWay
	 *
	 * @param <T> - Any kind of object
	 * @param <U> - Any kind of object
	 */
	public interface IGraphQuery<T, U> extends IDone {
		public void find(T t);

		public U getValue();

		public void reset();
	}
}
