package com.devnull.gway.entity;

import java.util.ArrayList;
import java.util.List;

/***
 * Objeto que contendrá la ruta mas corta, el resultado de la búsqueda y distancia
 * @author esalinas
 *
 */
public class ResponseRoute {
	private String result = "NoOk";
	private String message = "";
	private Double distance = 0.0;
	private List<ResponsePoint> points = new ArrayList<>();

	public ResponseRoute() {
	}

	public ResponseRoute(String result, String message, List<ResponsePoint> points) {
		this.result = result;
		this.message = message;
		this.points = points;
	}

	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public List<ResponsePoint> getPoints() {
		return points;
	}

	public void setPoints(List<ResponsePoint> points) {
		this.points = points;
	}

	public Double getDistance() {
		return distance;
	}

	public void setDistance(Double distance) {
		this.distance = distance;
	}
}