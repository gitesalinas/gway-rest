package com.devnull.gway.entity;

/***
 * Objeto donde se recupera latitud y longitud de los 
 * puntos inicio y final que llega al método Rest
 */
public class LatLon {
	private double sourceLat;
	private double sourceLon;

	private double targetLat;
	private double targetLon;

	public LatLon() {
	}

	public double getSourceLat() {
		return sourceLat;
	}

	public void setSourceLat(double sourceLat) {
		this.sourceLat = sourceLat;
	}

	public double getSourceLon() {
		return sourceLon;
	}

	public void setSourceLon(double sourceLon) {
		this.sourceLon = sourceLon;
	}

	public double getTargetLat() {
		return targetLat;
	}

	public void setTargetLat(double targetLat) {
		this.targetLat = targetLat;
	}

	public double getTargetLon() {
		return targetLon;
	}

	public void setTargetLon(double targetLon) {
		this.targetLon = targetLon;
	}
}
