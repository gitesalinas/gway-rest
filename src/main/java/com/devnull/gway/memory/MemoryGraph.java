package com.devnull.gway.memory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.devnull.gway.dao.GrafoDao;
import com.devnull.gway.entity.Graph;

/***
 *  Clase que guardara en memoria el grafo que representará 
 *  la topologia de carreteras, vías y pistas de Perú
 */
@Component
@Scope("singleton")
public class MemoryGraph {
	
	public MemoryGraph() {  }
	
	/***
	 * Grafo
	 */
	private Graph graph;

	
	/***
	 * Inyección de dependencias 
	 */
	@Autowired
	/***
	 * Objeto de acceso a datos para lectura de Shape Files
	 */
	private GrafoDao dao;
	
	
	public Graph getGraph() {
		return graph;
	}
	
	public Graph setGraph() {
		return graph;
	}
	
	public void load() throws Exception {
		
		if(null != this.graph) return;
		
		System.out.println("Start Building Graph");
		
		this.graph = dao.loadGraph();
    	
    	System.out.println("Name: " + this.graph.getName());
    	System.out.println("Nodes: " + this.graph.getNodes().size());
    	System.out.println("Edges: " + this.graph.getEdges().size());
    	
    	System.out.println("Graph Built");
	}
}
