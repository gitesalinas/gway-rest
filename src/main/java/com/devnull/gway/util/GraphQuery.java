package com.devnull.gway.util;



import com.devnull.gway.entity.Edge;
import com.devnull.gway.entity.Graph;
import com.vividsolutions.jts.geom.LineString;
import com.vividsolutions.jts.geom.Point;

/**
 * Factory de operaciones en grafo
 */
public class GraphQuery {
	
	/***
	 * Búsqueda de aristas cercanas a punto inicio y objetivo 
	 */
	public static Graph.IGraphQuery<Edge, Edge[]> getQueryFindGraphEdgesBySourceAndTargetPoints(
			final Point sourcePoint, final Point targetPoint) {
		
		return new Graph.IGraphQuery<Edge, Edge[]>() {
			
			private Double bestSourceDistance = Double.MAX_VALUE;
			private Double bestTargetDistance = Double.MAX_VALUE;
			private Edge[] bestEdges = new Edge[2];
			

			@Override
			public boolean isDone() { return false; }

			@Override
			public void find(Edge edge) {
				
				if(Edge.REVERSE.equals(edge.getDirection())) return;
				
				LineString lineString = (LineString)edge.getObject();
				Double sourceDistance = lineString.distance(sourcePoint);
				Double targetDistance = lineString.distance(targetPoint);
				
				if(bestSourceDistance > sourceDistance) {  
					bestSourceDistance = sourceDistance;
					bestEdges[0] = edge;
				}
				
				if(bestTargetDistance > targetDistance) {  
					bestTargetDistance = targetDistance;
					bestEdges[1] = edge;
				}
			}

			@Override
			public Edge[] getValue() { return bestEdges; }

			@Override
			public void reset() {
				bestSourceDistance = Double.MAX_VALUE;
				bestTargetDistance = Double.MAX_VALUE;
				bestEdges = new Edge[2];
			}
		};
	}
}
