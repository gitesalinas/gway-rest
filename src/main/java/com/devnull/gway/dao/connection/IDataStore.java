package com.devnull.gway.dao.connection;

import org.geotools.data.DataStore;
import org.geotools.data.FeatureSource;
import org.geotools.feature.FeatureCollection;
import org.geotools.feature.FeatureIterator;
import org.springframework.stereotype.Component;

public interface IDataStore {
	DataStore getDataStore() throws Exception;
	FeatureSource<?, ?> getFeatureSource() throws Exception;
	FeatureCollection<?, ?> getFeatureCollection() throws Exception;
	FeatureIterator<?> getFeatureIterator() throws Exception;
	void dispose();
}
