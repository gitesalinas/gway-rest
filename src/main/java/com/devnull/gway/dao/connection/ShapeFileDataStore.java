package com.devnull.gway.dao.connection;

import java.io.File;

import org.geotools.data.DataStore;
import org.geotools.data.FeatureSource;
import org.geotools.data.FileDataStore;
import org.geotools.data.FileDataStoreFinder;
import org.geotools.feature.FeatureCollection;
import org.geotools.feature.FeatureIterator;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

/***
 * Accesso a datos para la lectura de ShapeFiles
 */
@Component
@Qualifier("ShapeFile")
public class ShapeFileDataStore implements IDataStore {

	private String path;
	private DataStore dataStore;
	private FeatureSource<?, ?> featureDataSource;
	private FeatureCollection<?, ?> featureCollection;
	
	public ShapeFileDataStore() {  
		this.path = "C:\\Users\\esalinas\\Downloads\\SF\\at_2po_4pgr.shp";
	}
	
	
	@Override
	public DataStore getDataStore() throws Exception {
		
		if(null == dataStore) {
			File file = new File(this.path);
			
			if(null == file) throw new Exception("Shape File Not Found");
			
			dataStore = FileDataStoreFinder.getDataStore(file);
		}
		
		return dataStore;	
	}

	@Override
	public FeatureSource<?, ?> getFeatureSource() throws Exception {
		if(null == featureDataSource) {
			featureDataSource = ((FileDataStore)getDataStore()).getFeatureSource();
		}
		return featureDataSource;
	}

	@Override
	public FeatureCollection<?, ?> getFeatureCollection() throws Exception {
		if(null == featureCollection) {
			featureCollection = getFeatureSource().getFeatures();
		}
		return featureCollection;
	}

	@Override
	public FeatureIterator<?> getFeatureIterator() throws Exception {
		return getFeatureCollection().features();
	}

	@Override
	public void dispose() {
		this.path = null;
		this.dataStore.dispose();
		this.dataStore = null;
		this.featureDataSource = null;
		this.featureCollection = null;
	}
}
