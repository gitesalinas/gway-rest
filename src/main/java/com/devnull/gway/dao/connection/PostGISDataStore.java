package com.devnull.gway.dao.connection;

import java.util.HashMap;
import java.util.Map;

import org.geotools.data.DataStore;
import org.geotools.data.DataStoreFinder;
import org.geotools.data.FeatureSource;
import org.geotools.feature.FeatureCollection;
import org.geotools.feature.FeatureIterator;
import org.geotools.jdbc.JDBCDataStoreFactory;
import org.geotools.util.UnsupportedImplementationException;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

/***
 * Accesso a datos para la lectura de base de datos Postgres
 */
@Component
@Qualifier("PostGIS")
public class PostGISDataStore implements IDataStore {

	private Map<String,Object> params;
	private DataStore dataStore;
	private FeatureCollection<?, ?> featureCollection;
	
	public PostGISDataStore() {
		
		this.params = new HashMap<>();
		
	    params.put( "dbtype", "postgis");
	    params.put( "host", "localhost");
	    params.put( "port", 5432);
	    params.put( "schema", "public");
	    params.put( "database", "osm_pgrouting");
	    params.put( "user", "postgres");
	    params.put( "passwd", "");
	    
	    params.put(JDBCDataStoreFactory.EXPOSE_PK.key, true);
        params.put(JDBCDataStoreFactory.PK_METADATA_TABLE.key, "gt_pk_metadata");;
	}
	
	@Override
	public DataStore getDataStore() throws Exception {
		
		if(null == dataStore) {
		    
		    dataStore = DataStoreFinder.getDataStore(this.params);
		}
	    
	    return dataStore;
	}

	@Override
	public FeatureSource<?, ?> getFeatureSource() throws Exception {
		throw new UnsupportedImplementationException("UnsupportedImplementationException");
	}

	@Override
	public FeatureCollection<?, ?> getFeatureCollection() throws Exception {
		if(null == featureCollection) {
			featureCollection = getFeatureSource().getFeatures();
		}
		return featureCollection;
	}

	@Override
	public FeatureIterator<?> getFeatureIterator() throws Exception {
		return getFeatureCollection().features();
	}

	@Override
	public void dispose() {
		this.params = null;
		this.dataStore.dispose();
		this.dataStore = null;
		this.featureCollection = null;
	}
}
