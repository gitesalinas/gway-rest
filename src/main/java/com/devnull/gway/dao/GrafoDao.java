package com.devnull.gway.dao;

import java.util.HashMap;
import java.util.concurrent.ConcurrentHashMap;

import org.geotools.feature.FeatureIterator;
import org.geotools.geometry.jts.JTSFactoryFinder;
import org.opengis.feature.simple.SimpleFeature;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import com.devnull.gway.dao.connection.IDataStore;
import com.devnull.gway.entity.Edge;
import com.devnull.gway.entity.Graph;
import com.devnull.gway.entity.Node;
import com.devnull.gway.entity.Segment;
import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.GeometryFactory;
import com.vividsolutions.jts.geom.LineString;
import com.vividsolutions.jts.geom.Point;

@Repository
public class GrafoDao {
	
	private static final Double REVERSE_COST_VALUE_INDICATOR = 1000000.0;
	private static final GeometryFactory GEOMETRY_FACTORY = JTSFactoryFinder.getGeometryFactory();
	
	private String ID = "ID";
	private String CLAZZ = "CLAZZ";
	private String FLAGS = "FLAGS";
	private String SOURCE = "SOURCE";
	private String TARGET = "TARGET";
	private String KM = "KM";
	private String KMH = "KMH";
	private String COST = "COST";
	private String REVERSE_COST= "REVERSE_CO";
	private String X1 = "X1";
	private String Y1 = "Y1";
	private String X2 = "X2";
	private String Y2 = "Y2";
	
	/***
	 * Inyección de dependecias
	 */
	@Autowired
	
	/***
	 * DataStore para la klectura de datos desde ShapeFiles
	 */
	@Qualifier("ShapeFile")
	
	/***
	 * Encargado de la lectura de ShapeFiles
	 */
	private IDataStore dataStore;
	
	
	private long nodeIds = 0;
	private long edgeIds = 0;
	
	private Graph graph;
	private ConcurrentHashMap<Long, Edge> edges;
	private ConcurrentHashMap<Long, Node> nodes;
	private HashMap<Coordinate, Node> coordinateWithNodes = new HashMap<>();
	
	
	public GrafoDao() {  
		this.graph = new Graph("Topología de carreteras, vías y pistas de Perú");
		this.edges = graph.getEdges();
		this.nodes = graph.getNodes();
	}
	
	public Graph loadGraph() throws Exception {
		
		System.out.println("Datastore used: " + dataStore.getClass().getSimpleName());
		
		FeatureIterator<?> iter = dataStore.getFeatureIterator();
		while (iter.hasNext()) {
			SimpleFeature feature = (SimpleFeature)iter.next();
			
			Segment segment = parseFeatureToSegment(feature);
			
			Coordinate firstCoordinate = new Coordinate(segment.getX1(), segment.getY1());
			Coordinate lastCoordinate = new Coordinate(segment.getX2(), segment.getY2());
			
			Node sourceNode = retrieveOrCreateNode(firstCoordinate);
			Node targetNode = retrieveOrCreateNode(lastCoordinate);
			
			createEdge(sourceNode, targetNode, segment);
		}
		
		iter.close();
		iter = null;
		dispose();
		
		return graph;
	}
	
	/***
	 * Create a objeto de tipo Segment que representa una arista del grafo
	 * @param feature es la representación geometrica de una línea) 
	 */
	private Segment parseFeatureToSegment(SimpleFeature feature) {
		Segment segment = new Segment();
		segment.setId(Integer.parseInt("" + feature.getAttribute(ID)));
		segment.setClazz(Integer.parseInt("" + feature.getAttribute(CLAZZ)));
		segment.setFlags(Integer.parseInt("" + feature.getAttribute(FLAGS)));
		segment.setSource(Integer.parseInt("" + feature.getAttribute(SOURCE)));
		segment.setTarget(Integer.parseInt("" + feature.getAttribute(TARGET)));
		segment.setKm((Double)feature.getAttribute(KM));
		segment.setKmh(Integer.parseInt("" + feature.getAttribute(KMH)));
		segment.setCost((Double)feature.getAttribute(COST));
		segment.setReverse_cost((double)feature.getAttribute(REVERSE_COST));
		segment.setX1((double)feature.getAttribute(X1));
		segment.setY1((double)feature.getAttribute(Y1));
		segment.setX2((double)feature.getAttribute(X2));
		segment.setY2((double)feature.getAttribute(Y2));
		return segment;
	}
	
	/***
	 * Recupera o crea un objeto de tipo Nodo. Si existe recupera el Nodo de coordinateWithNodes
	 * en caso contrario crea un nuevo nodo y lo agrega a coordinateWithNodes
	 * @param coordinate Coordenada geometrica de un punto
	 */
	private Node retrieveOrCreateNode(Coordinate coordinate) {
		Node node = coordinateWithNodes.get(coordinate);
		
		if(node != null) return node;
		
		node = createNode(coordinate);
		
		return node;
	}
	
	/***
	 * Crea un objeto de tipo Nodo y lo agrega a coordinateWithNodes
	 * @param coordinate Coordenada geometrica de un punto
	 */
	private Node createNode(Coordinate coordinate) {
		Node node = new Node(nodeIds);
		node.setObject(createPoint(coordinate));
		nodes.put(nodeIds, node);
		coordinateWithNodes.put(coordinate, node);
		nodeIds++;
		
		return node;
	}
	
	/***
	 * Crea un objeto de tipo Edge. Verifica si la via es de doble sentido.
	 * Si la vía es doble sentido crea un nuevo Edge y marca el Edge 
	 * como bidireccional (vía de doble sentido)
	 * @param sourceNode Nodo inicio creado previamente
	 * @param targetNode Nodo objetivo creado previamente
	 * @param segment Información del segmento (arista o línea)
	*/
	private Edge createEdge(Node sourceNode, Node targetNode, Segment segment) {
		
		Edge edge = new Edge(edgeIds, sourceNode, targetNode, segment.getKm());
		
		Point pointSource = (Point)sourceNode.getObject();
		Point pointTarget = (Point)targetNode.getObject();
		
		edge.setObject(createLineString(pointSource.getCoordinate(), pointTarget.getCoordinate()));
		
		edge.setData(segment);
		
		edges.put(edgeIds, edge);
		
		edge.getSourceNode().addEdge(edge);
		
		edgeIds++;
		
		/***
		 * Verificar si es de doble sentido la vía
		 */
	    if (!segment.getReverse_cost().equals(REVERSE_COST_VALUE_INDICATOR) && !edge.getSourceNode().equals(edge.getTargetNode())) {
	    	
	    	Edge reverseEdge = new Edge(edgeIds, targetNode, sourceNode, segment.getKm());
	    	
	    	reverseEdge.setObject(createLineString(pointTarget.getCoordinate(), pointSource.getCoordinate()));
	    	
	    	reverseEdge.setData(segment);
	    	
	    	reverseEdge.setIsBidirectional();
	    	
	    	reverseEdge.setDirection(Edge.REVERSE);
	    	
	    	edges.put(edgeIds, reverseEdge);
	    	
	    	edge.getTargetNode().addEdge(reverseEdge);
	    	
	    	edge.setIsBidirectional();
	    	
	    	edgeIds++;
	    }
	    
		return edge;
	}
	
	/***
	 * Metodo helper para crear un objeto de tipo Point
	 * @param coordinate Coordenada del punto
	 * @return
	 */
	private Point createPoint(Coordinate coordinate) {
		return GEOMETRY_FACTORY.createPoint(coordinate);
	}
	
	/***
	 * Metodo helper para crear un objeto de tipo LineString
	 * @param sourceCoordinate Coordenada del punto inicio
	 * * @param targetCoordinate Coordenada del punto objetivo
	 * @return
	 */
	private LineString createLineString(Coordinate sourceCoordinate, Coordinate targetCoordinate) {
		return GEOMETRY_FACTORY.createLineString(new Coordinate[] { sourceCoordinate, targetCoordinate });
	}
	
	/***
	 * Limpiar memoria
	 */
	private void dispose() {
		this.dataStore.dispose();
		this.dataStore = null;
		this.coordinateWithNodes.clear();
		this.coordinateWithNodes = null;
	}
}
