/***
 * Controlador encargado de mostrar la página inicial
 */

package com.devnull.gway.controllers.web;

import org.springframework.web.servlet.ModelAndView;

import com.devnull.gway.memory.MemoryGraph;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class HomeController {
	
	/***
	 * Inyección de dependecias
	 */
	@Autowired
	/***
	 * Acceso a única instancia donde se encuentra grafo con
	 * con la topología de carreteras, vías y pistas de Perú
	 */
	private MemoryGraph memoryGraph;
	
	/***
	 * Url path para acceder a la acceder a la web
	 */
    @RequestMapping("/")
    public ModelAndView home() {
    	ModelAndView modelAndView = new ModelAndView("index");
    
    	modelAndView.addObject("GraphName", memoryGraph.getGraph().getName());
    	
        return modelAndView;
    }
}
