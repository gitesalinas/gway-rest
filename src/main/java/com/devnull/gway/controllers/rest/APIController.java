package com.devnull.gway.controllers.rest;


import org.geotools.geometry.jts.JTSFactoryFinder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.devnull.gway.client.Routes;
import com.devnull.gway.entity.Graph;
import com.devnull.gway.entity.LatLon;
import com.devnull.gway.entity.ResponseRoute;
import com.devnull.gway.memory.MemoryGraph;
import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.GeometryFactory;
import com.vividsolutions.jts.geom.Point;;

@RestController
public class APIController {

	/***
	 * Fabrica de objetos geométricos util para cálculos de distancia entre puntos, lineas
	 */
	public final static GeometryFactory GEOMETRYFACTORY = JTSFactoryFinder.getGeometryFactory();
	
	/***
	 * Inyección de dependecias
	 */
	@Autowired
	/***
	 * Acceso a única instancia donde se encuentra grafo con
	 * con la topología de carreteras, vías y pistas de Perú
	 */
	private MemoryGraph memoryGraph;

    @PostMapping(path = "/ruta-mas-corta")
    public ResponseEntity<?> shortestPath(@RequestBody LatLon latLon) throws Exception {
    	
    	/***
    	 * Crea punto de inicio (Longitud, Latitud)
    	 */
    	Point sourcePoint = GEOMETRYFACTORY.createPoint(new Coordinate(latLon.getSourceLon(), latLon.getSourceLat()));
    	/***
    	 * Crea punto de objetivo (Longitud, Latitud)
    	 */
    	Point targetPoint = GEOMETRYFACTORY.createPoint(new Coordinate(latLon.getTargetLon(), latLon.getTargetLat()));
		
    	/***
    	 * Obtener referencia de grafo en memoria
    	 */
		Graph graph = memoryGraph.getGraph();
		
		/***
		 * Clase principal donde se realiza una serie de metodos:
		 * 1) Busca en el grafo las aristas mas cercanas a los puntos de inicio y objetivo - calculateEdgesNearestToPoints();
		 * 2) Correr algoritmo - runAlgorithm();
		 * 3) Cálculo de los nuevos puntos de inicio y objetivo - calculateNewPoints();
		 * 4) Ordena los nuevos puntos de inicio y objetivo dentro del objeto de respuesta - resolveNewPoints();
		 */
		Routes route = new Routes(graph, sourcePoint, targetPoint);
		
		return new ResponseEntity<ResponseRoute>(route.execute(), HttpStatus.OK);
    }
}
