package com.devnull.gway.structure;

import java.util.Comparator;
import java.util.HashMap;

import org.jdom2.internal.ArrayCopy;

/**
 * Binary Heap Template
 * Usado para al cola de prioridad
 * Data Structures: Heaps
 * https://www.youtube.com/watch?v=t0Cq6tVNRBA&t=203s
 */
public class BinaryHeap<E> {
	
	private Comparator<E> comparator;
	
	private HashMap<E, Integer> indexes = new HashMap<>();
	
	private final static int INCREASE_SIZE = 100;
	
	private E[] items;
	private int size = 0;
	private int capacity = 50;
	
	
	public BinaryHeap() {
		this(null);
	}
	
	public BinaryHeap(Comparator<E> comparator) {
		this.items = (E[]) new Object[capacity];
		this.comparator = comparator;
	}
	
	
	private boolean hasParent(int childIndex) {
		if(0 == childIndex) return false;
		return getParentIndex(childIndex) >= 0;
	}
	
	private boolean hasLeftChild(int parentIndex) {
		return getLeftChildIndex(parentIndex) < size;
	}
	
	private boolean hasRightChild(int parentIndex) {
		return getRightChildIndex(parentIndex) < size;
	}
	
	private int getParentIndex(int childIndex) {
		return (childIndex - 1) / 2;
	}
	
	private int getLeftChildIndex(int parentIndex) {
		return 2 * parentIndex + 1;
	}
	
	private int getRightChildIndex(int parentIndex) {
		return 2 * parentIndex + 2;
	}
	
	private E getParent(int index) {
		return items[index];
	}
	
	private E getLeftChild(int index) {
		return items[index];
	}
	
	private E getRightChild(int index) {
		return items[index];
	}
	
	private void swapItems(int indexOne, int indexTwo) {
		E temp = items[indexOne];
		items[indexOne] = items[indexTwo];
		items[indexTwo] = temp;
		
		indexes.put(items[indexOne], indexOne);
		indexes.put(items[indexTwo], indexTwo);
	}
	
	private void ensureExtraCapacity() {
		if(size < capacity) return;
		
		items = ArrayCopy.copyOf(items, (capacity + INCREASE_SIZE));
		capacity += INCREASE_SIZE;
	}
	
	private void heapifyUp(int index) {
		//int index = size - 1;
		
		while(hasParent(index)) {
			int parentIndex = getParentIndex(index);
			E o1 = getParent(parentIndex);
			E o2 = items[index];
			
			Integer result = this.comparator.compare(o1, o2);
			if(!result.equals(1)) break;
			
			swapItems(parentIndex, index);
			index = parentIndex;
		}
	}
	
	private void heapifyDown(int index) {
		//int index = 0;
		while(hasLeftChild(index)) {
			int smallChildIndex = getLeftChildIndex(index);
			if(hasRightChild(index)) {
				int rightChildIndex = getRightChildIndex(index);
				
				E o1 = getLeftChild(smallChildIndex);
				E o2 = getRightChild(rightChildIndex);
				
				Integer result = this.comparator.compare(o1, o2);
				if(result.equals(1)) {
					smallChildIndex = rightChildIndex;
				}
			}
			
			E o1 = items[index];
			E o2 = items[smallChildIndex];
			
			Integer result = this.comparator.compare(o1, o2);
			if(result != 1) break;
			
			swapItems(index, smallChildIndex);
			index = smallChildIndex;
		}
	}
	
	
	
	protected void push(E e) {
		ensureExtraCapacity();
		items[size] = e;
		indexes.put(e, size);
		size++;
		heapifyUp((size - 1));
	}
	
	protected E peek() throws Exception {
		if(0 == size) throw new Exception("Trying to remove item from empty array");
		return items[0];
	}
	
	protected E poll() throws Exception {
		if(0 == size) throw new Exception("Trying to remove item from empty array");
		
		E e = items[0];
		indexes.remove(items[0]);
		items[0] = null;
		size--;
		
		if(size > 0) {
			items[0] = items[size];
			indexes.remove(items[size]);
			items[size] = null;
			indexes.put(items[0], 0);
			heapifyDown(0);
		}
		
		return e;
	}

	protected int size() { return size; }

	protected boolean isEmpty() { return size == 0; }
	
	protected void relax(E e) {
		int index = indexes.get(e);
		heapifyUp(index);
		//index = indexes.get(e);
		//heapifyDown(index);
	}
}
