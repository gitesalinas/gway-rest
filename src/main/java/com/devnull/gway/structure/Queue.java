package com.devnull.gway.structure;

import java.util.Comparator;

/**
 * Custom Priority Queue Wrapper
 */
public class Queue<E> extends BinaryHeap<E> {
	
	public Queue(Comparator<E> comparator) {
		super(comparator);
	}
	
	public void push(E e) { super.push(e); }
	public E peek() { try { return super.peek(); } catch(Exception ex) { return null; } }
	public E poll() {  try { return super.poll(); } catch(Exception ex) { return null; } }
	public int size() { return super.size(); }
	public boolean isEmpty() { return super.isEmpty(); }
	public void relax(E e) { super.relax(e); }
}