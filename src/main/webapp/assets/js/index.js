var map, sourceMarker, targetMarker, polyline, $loading;

function run(t, a, e, r) {
    return $loading.show(), $.ajax({
        url: "/ruta-mas-corta",
        type: "POST",
        contentType: "application/json",
        data: JSON.stringify({
            sourceLat: t,
            sourceLon: a,
            targetLat: e,
            targetLon: r
        })
    }).done(function(t, a, e) {
        onSuccess.call(this, t, a, e)
    }).fail(function(t, a, e) {
        onError.call(this, t, a, e)
    }).always(function() {
        $loading.hide()
    });
};

function map() {
    map = L.map("map").setView([-8.105012, -79.028607], 15), L.tileLayer("https://tile.openstreetmap.org/{z}/{x}/{y}.png", {
        attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors',
        maxZoom: 18
    }).addTo(map);
    var r = new L.icon({
            iconUrl: "/assets/img/m-source.png",
            iconSize: [64, 64],
            iconAnchor: [32, 59],
            popupAnchor: [-3, -76]
        }),
        e = new L.icon({
            iconUrl: "/assets/img/m-target.png",
            iconSize: [64, 64],
            iconAnchor: [32, 62],
            popupAnchor: [-3, -76]
        });
    sourceMarker = L.marker([-8.105012, -79.028607], {
        draggable: "true",
        icon: r
    }).addTo(map), targetMarker = L.marker([-8.106414, -79.026331], {
        draggable: "true",
        icon: e
    }).addTo(map), sourceMarker.on("dragend", function(t) {
        var a = t.target,
            e = a.getLatLng();
        a.setLatLng(new L.LatLng(e.lat, e.lng), {
            draggable: "true",
            icon: r
        }), map.panTo(new L.LatLng(e.lat, e.lng)), notifyChange()
    }), targetMarker.on("dragend", function(t) {
        var a = targetMarker.getLatLng();
        targetMarker.setLatLng(a, {
            draggable: "true",
            icon: e
        }), notifyChange()
    }), sourceMarker.bindPopup("<b>Inicio</b>").openPopup(), targetMarker.bindPopup("<b>Objetivo</b>").openPopup()
};

function notifyChange() {
    $("#slatitud-slongitud").html(parseFloat(sourceMarker.getLatLng().lat).toFixed(6) + ", " + parseFloat(sourceMarker.getLatLng().lng).toFixed(6)), $("#tlatitud-tlongitud").html(parseFloat(targetMarker.getLatLng().lat).toFixed(6) + ", " + parseFloat(targetMarker.getLatLng().lng).toFixed(6)), run(sourceMarker.getLatLng().lat, sourceMarker.getLatLng().lng, targetMarker.getLatLng().lat, targetMarker.getLatLng().lng)
};
$(function() {
    $loading = $("#dvloading"), map(), notifyChange()
});