<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<title>Dijkstra - Shortest Path</title>

<meta charset="utf-8" />

<link rel="stylesheet" href="/assets/css/leaflet.css"  />
	
<link rel="stylesheet" href="/assets/css/bootstrap.min.css" />
<link rel="stylesheet" href="/assets/css/bootstrap-theme.min.css" />
<link rel="stylesheet" href="/assets/css/index.css" />

</head>
<body>

	<div id="container" class="container">
		<h2 class="text-center">
			Dijkstra - Shortest Path <br />
			<small>${GraphName}</small>
		</h2>
		
		<hr />
		
		<div class="row">
			<div class="col-md-3">
				<div id="detail">
					<div class="col-md-12 item">
						<h5 class="text-center"><strong>Punto Inicio</strong></h5>
						<p class="text-center"><strong>Latitud, Longitud:</strong> <br /> <span id="slatitud-slongitud"></span></p>
					</div>
					<div class="col-md-12 item">
						<h5 class="text-center"><strong>Punto Objetivo</strong></h5>
						<p class="text-center"><strong>Latitud, Longitud:</strong> <br /> <span id="tlatitud-tlongitud"></span></p>
					</div>
					
					<div class="col-md-12 item">
						<h5 class="text-center"><strong>Ruta</strong></h5>
						<p class="text-center"><strong>Distancia:</strong> <span id="distance"></span></p>
					</div>
					
					<div class="col-md-12 item">
						<h5 class="text-center"><strong>Request</strong></h5>
						<p class="text-center">
							<strong>Request Url:</strong> 
							POST (${pageContext.request.contextPath}/ruta-mas-corta)<br />
						</p>
						<p class="text-center">
							<strong>Body:</strong><br />
							<small class="text-center">
					            <strong>sourceLat</strong> -> Latitud Inicio<br />
					            <strong>sourceLon</strong> -> Longitud Inicio<br />
					            <strong>targetLat</strong> -> Latitud Objetivo<br />
					            <strong>targetLon </strong>-> Longitud Objetivo
							</small>
						</p>
					</div>
				</div>
			</div>
			
			<div id="wrap-map" class="col-md-9">
				<div id="map" class="map"></div>
				<div id="dvloading" class="loading" style="display:none;"></div>
			</div>
		
		</div>
		
		
	</div>

	


	<script src="/assets/js/jquery.js"></script>
	<script src="/assets/js/leaflet.js"></script>
	<script src="/assets/js/index.js"></script>

	<script type="text/javascript">
	
		function onSuccess(data, textStatus, jqXHR) {

			if (polyline)
				polyline.remove();

			var points = data.points;

			var pointList = [];

			for (var i = 0; i < points.length; i++) {
				pointList.push(new L.LatLng(points[i].latitude,
						points[i].longitude));
			}

			polyline = new L.Polyline(pointList, {
				color : 'black',
				weight : 4,
				opacity : 0.7,
				smoothFactor : 1
			});

			polyline.addTo(map);
			
			$("#distance").html(data.distance + " KM");

		}
		
		function onError(jqXHR, textStatus, errorThrown) {
			console.log(jqXHR.responseText);
		}
		
	</script>
</body>
</html>